import "./style.css";

const Timer = ({ value }) => (
	<span className="timer">{value ? value : "time is over"}</span>
);

export default Timer;
