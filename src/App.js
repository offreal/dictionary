import "./App.css";
import { useCallback, useState, useRef, useEffect } from "react";
import Switch from "@material-ui/core/Switch";
import Badge from "@material-ui/core/Badge";
import { AccountBalance } from "@material-ui/icons";
import dictionary from "./constants/dictionary.js";

import { Timer } from "./components";

const optionsAmount = 8;
const COUNTER_VALUE = 7;

let dictionaryStore; // = [...dictionary];

const getNewAnswerPosition = () =>
	Math.round(Math.random() * (optionsAmount - 1));

const initialAnswerPosition = getNewAnswerPosition();

const App = () => {
	const [wordIndex, setWordIndex] = useState(null);
	const [answerPosition, setAnswerPosition] = useState(initialAnswerPosition);
	const [isEn, setEn] = useState(true);
	const [wrong, setWrong] = useState(0);
	const [right, setRight] = useState(0);
	const [prevResult, setPrevResult] = useState(null);
	const [counter, setCounter] = useState(COUNTER_VALUE);
	const timerId = useRef(null);
	const counterRef = useRef(null);

	const reduceCounter = useCallback(() => {
		setCounter((counter) => counter && counter - 1);
	}, [setCounter]);

	useEffect(() => {
		counterRef.current = counter;
		if (counter) {
			clearTimeout(timerId.current);
			timerId.current = setTimeout(reduceCounter, 1000);
		}
	}, [counter, reduceCounter]);

	useEffect(() => {
		const indexedDictionaryStore = localStorage.getItem("dictionary");
		const wrong = localStorage.getItem("wrong");
		const right = localStorage.getItem("right");
		wrong && setWrong(+wrong);
		right && setRight(+right);
		if (indexedDictionaryStore) {
			dictionaryStore = indexedDictionaryStore
				.split(",")
				.map((key) => dictionary.find((item) => item.key === +key));
		} else {
			dictionaryStore = [...dictionary];
		}
		setWordIndex(
			new Array(optionsAmount).fill(null).map((item, i) =>
				getRandomWordIndex(
					i,
					// initialAnswerPosition === i ? dictionaryStore : dictionary
					initialAnswerPosition === i
				)
			)
		);
	}, [setWordIndex]);

	const toggleEn = useCallback(() => {
		setEn((isEn) => !isEn);
	}, [setEn]);

	const handleOnClick = useCallback(
		(key) => {
			let choosed;
			if (
				dictionary[wordIndex[key]][isEn ? "ru" : "en"] ===
				dictionary[wordIndex[answerPosition]][isEn ? "ru" : "en"]
			) {
				choosed = dictionary[wordIndex[key]][isEn ? "ru" : "en"];

				if (counterRef.current) {
					if (dictionaryStore.length <= 1) {
						alert("Contgrats, You are finished - Aplication will be restarted");
						["dictionary", "wrong", "right"].forEach((item) =>
							localStorage.removeItem(item)
						);
						dictionaryStore = [...dictionary];
					} else {
						const delIndex = dictionaryStore.findIndex(
							({ key }) => key === dictionary[wordIndex[answerPosition]].key
						);

						dictionaryStore.splice(delIndex, 1);
						localStorage.setItem(
							"dictionary",
							dictionaryStore.map(({ key }) => key)
						);
						setRight((right) => {
							localStorage.setItem("right", right + 1);
							return right + 1;
						});
					}
					// if (counterRef.current) {

					// }
				}
			} else {
				choosed = `${dictionary[wordIndex[key]][isEn ? "ru" : "en"]} - ${
					dictionary[wordIndex[key]][isEn ? "en" : "ru"]
				}`;

				setWrong((wrong) => {
					localStorage.setItem("wrong", wrong + 1);

					return wrong + 1;
				});
			}
			setPrevResult({
				word: dictionary[wordIndex[answerPosition]][isEn ? "en" : "ru"],
				choosed,
				right: dictionary[wordIndex[answerPosition]][isEn ? "ru" : "en"],
				inTime: Boolean(counterRef.current),
			});

			const updatedAnswerPosition = getNewAnswerPosition();
			setAnswerPosition(updatedAnswerPosition);
			setWordIndex(
				new Array(optionsAmount).fill(null).map((item, i) =>
					getRandomWordIndex(
						i,
						updatedAnswerPosition === i // ? dictionaryStore : dictionary
					)
				)
			);
			setCounter(COUNTER_VALUE);
		},
		[answerPosition, wordIndex, isEn]
	);

	return (
		<div className="App">
			<div className="result">
				<Badge badgeContent={wrong} max={999} color="secondary">
					<Badge
						anchorOrigin={{
							vertical: "top",
							horizontal: "left",
						}}
						badgeContent={right}
						max={999}
						color="primary"
					>
						<AccountBalance />
					</Badge>
				</Badge>
			</div>
			{wordIndex ? (
				<>
					<h3>{`${dictionary[wordIndex[answerPosition]][isEn ? "en" : "ru"]}${
						isEn && dictionary[wordIndex[answerPosition]].transcript
							? " - [" + dictionary[wordIndex[answerPosition]].transcript + "]"
							: ""
					}`}</h3>
					<div className="test">
						{new Array(optionsAmount).fill(null).map((e, key) => (
							<TestOption
								key={key}
								onClick={() => handleOnClick(key)}
								text={
									key === answerPosition
										? dictionary[wordIndex[answerPosition]][isEn ? "ru" : "en"]
										: dictionary[wordIndex[key]][isEn ? "ru" : "en"]
								}
							/>
						))}
					</div>{" "}
				</>
			) : (
				"loading..."
			)}

			<div className={`timer_container${counter ? " green" : " red"}`}>
				<Timer value={counter} />
			</div>
			<div
				className={`prevResult ${
					prevResult &&
					(prevResult.choosed === prevResult.right ? "" : "isWrong")
				}`}
			>
				<table>
					<tbody>
						<tr>
							<td>Word:</td>
							<td>{prevResult && prevResult.word}</td>
						</tr>
						<tr>
							<td>You answer:</td>
							<td>{prevResult && prevResult.choosed}</td>
						</tr>
						<tr>
							<td>Right answer:</td>
							<td>{prevResult && prevResult.right}</td>
						</tr>
						<tr className={prevResult && (prevResult.inTime ? "" : "red")}>
							<td>In time:</td>
							<td>{prevResult && prevResult.inTime.toString()}</td>
						</tr>
						<tr>
							<td>Progress:</td>
							<td>
								{dictionaryStore &&
									Math.round(
										((dictionary.length - dictionaryStore.length) * 10000) /
											dictionary.length
									) / 100}{" "}
								%
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div className="settings">
				<div>en</div>
				<div>
					<Switch checked={!isEn} onChange={toggleEn} />
				</div>
				<div>ru</div>
			</div>
		</div>
	);
};

export default App;

const getRandomWordIndexCash = [];

function getRandomWordIndex(i, isAnswer) {
	const curDictionary = isAnswer ? dictionaryStore : dictionary;
	let w = Math.round(Math.random() * (curDictionary.length - 1));
	if (
		i &&
		curDictionary.length >= 7 &&
		getRandomWordIndexCash.slice(0, i).some((v) => v === curDictionary[w].key)
	) {
		return getRandomWordIndex(i, isAnswer);
	}
	getRandomWordIndexCash[i] = curDictionary[w].key;
	return curDictionary[w].key;
}

const TestOption = ({ text, onClick }) => {
	return (
		<div className="testOption" onClick={onClick}>
			{text}
		</div>
	);
};
